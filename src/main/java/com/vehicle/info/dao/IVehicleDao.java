package com.vehicle.info.dao;

import java.util.List;

import com.vehicle.info.entities.Vehicle;

public interface IVehicleDao {
	
	public void persist(Vehicle vehicle);
	
	public Vehicle merge(Vehicle vehicle);
	
	public Vehicle findById(Long id);
	
	public List<Vehicle> findAll();
	
}
