package com.vehicle.info.dao;

import java.util.List;

import com.vehicle.info.entities.OwnershipHostory;

public interface IOwnershipHistoryDao {

	public void persit(OwnershipHostory ownershipHostory);
	
	public List<OwnershipHostory> findOwnerships(Long vehicleId);
	
}
