package com.vehicle.info.dao;

import java.util.List;

import com.vehicle.info.entities.MaintenanceHistory;

public interface IMaintenanceHistoryDao {
	
	public MaintenanceHistory merge(MaintenanceHistory maintenanceHistory);
	
	public MaintenanceHistory findById(Long maintenanceId);
	
	public List<MaintenanceHistory> findListByVehicleId(Long vehicleId);

}
