package com.vehicle.info.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.vehicle.info.dao.IOwnershipHistoryDao;
import com.vehicle.info.entities.OwnershipHostory;

@Repository
public class OwnershipHistoryDao implements IOwnershipHistoryDao {

	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public void persit(OwnershipHostory ownershipHostory) {
		 entityManager.persist(ownershipHostory);
	}

	@Override
	public List<OwnershipHostory> findOwnerships(Long vehicleId) {
		return entityManager
				.createQuery("from OwnershipHostory where vehicle.id =:vehicleId order by id desc", OwnershipHostory.class)
				.setParameter("vehicleId", vehicleId)
				.getResultList();
	}

}
