package com.vehicle.info.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.vehicle.info.dao.IUserDao;
import com.vehicle.info.entities.User;

@Repository
public class UserDao implements IUserDao{

	@Autowired
	EntityManager entityManager;

	@Override
	public void persist(User user) {
		entityManager.persist(user);
	}

	@Override
	public User merge(User user) {
		return entityManager.merge(user);
	}

	@Override
	public List<User> findUsers() {
		return entityManager.createQuery("from User", User.class).getResultList();
	}

	@Override
	public User findById(Long id) {
		return entityManager.find(User.class, id);
	}
	
	@Override
	public User findByUsername(String username) {
		return entityManager.createQuery("from User where username=:username", User.class)
				.setParameter("username", username)
				.getSingleResult();
	}
	
	
}
