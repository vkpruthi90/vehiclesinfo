package com.vehicle.info.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.vehicle.info.dao.IMaintenanceHistoryDao;
import com.vehicle.info.entities.MaintenanceHistory;

@Repository
public class MaintenanceHistoryDao implements IMaintenanceHistoryDao{

	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public MaintenanceHistory merge(MaintenanceHistory maintenanceHistory) {
		return entityManager.merge(maintenanceHistory);
	}

	@Override
	public List<MaintenanceHistory> findListByVehicleId(Long vehicleId) {
		return entityManager
				.createQuery("from MaintenanceHistory where vehicle.id=:vehicleId and isActive=true", MaintenanceHistory.class)
				.setParameter("vehicleId", vehicleId)
				.getResultList();
	}

	@Override
	public MaintenanceHistory findById(Long maintenanceId) {
		return entityManager.find(MaintenanceHistory.class, maintenanceId);
	}

}
