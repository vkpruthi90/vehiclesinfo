package com.vehicle.info.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.vehicle.info.dao.IVehicleDao;
import com.vehicle.info.entities.Vehicle;

@Repository
public class VehicleDao implements IVehicleDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void persist(Vehicle vehicle) {
		entityManager.persist(vehicle);
	}
	
	@Override
	public Vehicle merge(Vehicle vehicle) {
		return entityManager.merge(vehicle);
	}

	@Override
	public Vehicle findById(Long id) {
		return entityManager.find(Vehicle.class, id);
	}

	@Override
	public List<Vehicle> findAll() {
		return entityManager.createQuery("from Vehicle", Vehicle.class).getResultList();
	}
	
}
