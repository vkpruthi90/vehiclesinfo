package com.vehicle.info.dao;

import java.util.List;

import com.vehicle.info.entities.User;

public interface IUserDao {
	
	public void persist(User user);
	
	public User merge(User user);
	
	public List<User> findUsers();
	
	public User findById(Long id);
	
	public User findByUsername(String username);
}
