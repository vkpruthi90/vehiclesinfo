package com.vehicle.info.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vehicle.info.dao.IMaintenanceHistoryDao;
import com.vehicle.info.dao.IUserDao;
import com.vehicle.info.dto.MaintenanceHistoryDTO;
import com.vehicle.info.entities.MaintenanceHistory;
import com.vehicle.info.exception.handler.HistoryNotFoundException;
import com.vehicle.info.services.IMaintenanceHistoryService;
import com.vehicle.info.services.IUserService;
import com.vehicle.info.services.IVehicleService;

@Service
@Transactional
public class MaintenanceHistoryService implements IMaintenanceHistoryService {

	@Autowired
	IMaintenanceHistoryDao maintenanceHistoryDao;
	
	@Autowired
	IUserService userService;
	
	@Autowired
	IVehicleService vehicleService;
	
	@Override
	public MaintenanceHistoryDTO merge(MaintenanceHistoryDTO maintenanceHistory) {
		MaintenanceHistory maintenanceHistory2 = new ModelMapper().map(maintenanceHistory, MaintenanceHistory.class);
		maintenanceHistory2.setUser(userService.findById(maintenanceHistory.getUser().getId()));
		maintenanceHistory2.setVehicle(vehicleService.findById( maintenanceHistory.getVehicle().getId()));
		maintenanceHistory2 = maintenanceHistoryDao.merge(maintenanceHistory2);
		return new ModelMapper().map(maintenanceHistory2, MaintenanceHistoryDTO.class);
	}
	
	@Override
	public MaintenanceHistoryDTO update(MaintenanceHistoryDTO maintenanceHistory, Long maintenanceId) {
		MaintenanceHistory maintenanceHistory1 = maintenanceHistoryDao.findById(maintenanceId);
		if(maintenanceHistory1 == null) {
			throw new HistoryNotFoundException(maintenanceId);
		}
		maintenanceHistory.setId(maintenanceId);
		MaintenanceHistory maintenanceHistory2 = maintenanceHistoryDao
				.merge(new ModelMapper().map(maintenanceHistory, MaintenanceHistory.class));
		return new ModelMapper().map(maintenanceHistory2, MaintenanceHistoryDTO.class);
	}
	
	@Override
	public void deleteByMaintenanceId(Long maintenanceId) {
		MaintenanceHistory maintenanceHistory = maintenanceHistoryDao.findById(maintenanceId);
		if(maintenanceHistory == null) {
			throw new HistoryNotFoundException(maintenanceId);
		}
		maintenanceHistory.setIsActive(false);
		maintenanceHistoryDao.merge(maintenanceHistory);
	}

	@Override
	public List<MaintenanceHistoryDTO> findListByVehicleId(Long vehicleId) {
		return maintenanceHistoryDao.findListByVehicleId(vehicleId)
			.stream()
			.map(maintenanceHistory -> new ModelMapper().map(maintenanceHistory, MaintenanceHistoryDTO.class))
			.collect(Collectors.toList());
	}

}
