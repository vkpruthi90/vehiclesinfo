package com.vehicle.info.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.vehicle.info.dao.IUserDao;
import com.vehicle.info.dto.UserDTO_1;
import com.vehicle.info.dto.UserResponseDTO;
import com.vehicle.info.entities.User;
import com.vehicle.info.services.IUserService;

@Service
@Transactional
public class UserService implements IUserService, UserDetailsService {

	@Autowired
	IUserDao userDao;
		
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User applicationUser = userDao.findByUsername(username);
        if (applicationUser == null) {
        	throw new UsernameNotFoundException(username);
        
        }
        return new org.springframework.security.core.userdetails.User(applicationUser.getUsername(), applicationUser.getPassword(),
        		new ArrayList<>());
	}

	@Override
	public UserResponseDTO findByUsername(String username) {
		return new ModelMapper().map(userDao.findByUsername(username), UserResponseDTO.class);
	}

	@Override
	public void persist(User user) {
		user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
		userDao.persist(user);
	}

	@Override
	public UserResponseDTO merge(UserDTO_1 user) {
		user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
		User user2 = userDao.merge(new ModelMapper().map(user, User.class));
		return new ModelMapper().map(user2, UserResponseDTO.class);
	}

	@Override
	public List<User> findUsers() {
		return userDao.findUsers();
	}

	@Override
	public User findById(Long id) {
		return userDao.findById(id);
	}

	@Override
	public UserDetails authUser(String username) {
		return loadUserByUsername(username);
	}

}
