package com.vehicle.info.services.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vehicle.info.dao.IOwnershipHistoryDao;
import com.vehicle.info.dto.OwnershipDTO;
import com.vehicle.info.dto.OwnershipHistoryDTO;
import com.vehicle.info.entities.OwnershipHostory;
import com.vehicle.info.entities.Vehicle;
import com.vehicle.info.services.IOwnershipHistoryService;

@Service
@Transactional
public class OwnershipHistoryService implements IOwnershipHistoryService {

	@Autowired
	IOwnershipHistoryDao ownershipHistoryDao;

	DateFormat format = new SimpleDateFormat("dd-MM-yyyy");

	@Override
	public void persit(OwnershipDTO ownershipHostory, Vehicle vehicle) {
		OwnershipHostory ownershipHostory2;
		try {
			ownershipHostory2 = new OwnershipHostory(ownershipHostory.getComments(),
					format.parse(ownershipHostory.getTransferredOn()), vehicle, vehicle.getOwner());
			ownershipHistoryDao.persit(ownershipHostory2);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<OwnershipHistoryDTO> findOwnerships(Long vehicleId) {
		return ownershipHistoryDao.findOwnerships(vehicleId).stream()
				.map(ownershipHistory -> new ModelMapper().map(ownershipHistory, OwnershipHistoryDTO.class))
				.collect(Collectors.toList());
	}

}
