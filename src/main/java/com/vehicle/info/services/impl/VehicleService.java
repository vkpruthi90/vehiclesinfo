package com.vehicle.info.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vehicle.info.dao.IVehicleDao;
import com.vehicle.info.dto.OwnershipDTO;
import com.vehicle.info.dto.VehicleDTO;
import com.vehicle.info.entities.User;
import com.vehicle.info.entities.Vehicle;
import com.vehicle.info.exception.handler.UserNotFoundException;
import com.vehicle.info.exception.handler.VehicleNotFoundException;
import com.vehicle.info.services.IOwnershipHistoryService;
import com.vehicle.info.services.IUserService;
import com.vehicle.info.services.IVehicleService;

@Service
@Transactional
public class VehicleService implements IVehicleService{

	@Autowired
	IVehicleDao vehicleDao;
	
	@Autowired
	IUserService userService;
	
	@Autowired
	IOwnershipHistoryService ownershipHistoryService;
	
	@Override
	public void persistVehicle(Vehicle vehicle) {
		vehicleDao.persist(vehicle);		
	}

	@Override
	public VehicleDTO merge(VehicleDTO vehicle) {
		Vehicle vehicle2 = new ModelMapper().map(vehicle, Vehicle.class);
		return new ModelMapper().map(vehicleDao.merge(vehicle2), VehicleDTO.class);
	}

	@Override
	public Vehicle findById(Long id) {
		return vehicleDao.findById(id);
	}

	@Override
	public List<Vehicle> findAll() {
		return vehicleDao.findAll();
	}

	@Override
	public VehicleDTO addNewOwner(OwnershipDTO ownership) {
		Vehicle vehicle = vehicleDao.findById(ownership.getVehicleId());
		if(vehicle == null) {
			throw new VehicleNotFoundException(ownership.getVehicleId());
		}
		User owner = userService.findById(ownership.getOwnerId());
		if(owner == null ) {
			throw new UserNotFoundException(ownership.getOwnerId());
		}
		vehicle.setOwner(owner);
		vehicle = vehicleDao.merge(vehicle);
		ownershipHistoryService.persit(ownership, vehicle);
		return new ModelMapper().map(vehicle, VehicleDTO.class);
	}
}
