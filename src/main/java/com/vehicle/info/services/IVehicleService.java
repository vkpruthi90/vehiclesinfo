package com.vehicle.info.services;

import java.util.List;

import com.vehicle.info.dto.OwnershipDTO;
import com.vehicle.info.dto.VehicleDTO;
import com.vehicle.info.entities.Vehicle;

public interface IVehicleService {

	public void persistVehicle(Vehicle vehicle);
	
	public VehicleDTO merge(VehicleDTO vehicle);
	
	public Vehicle findById(Long id);
	
	public List<Vehicle> findAll();
	
	public VehicleDTO addNewOwner(OwnershipDTO vehicle);
}
