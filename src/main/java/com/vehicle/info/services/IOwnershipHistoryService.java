package com.vehicle.info.services;

import java.util.List;

import com.vehicle.info.dto.OwnershipDTO;
import com.vehicle.info.dto.OwnershipHistoryDTO;
import com.vehicle.info.entities.OwnershipHostory;
import com.vehicle.info.entities.Vehicle;


public interface IOwnershipHistoryService {
	
	public void persit(OwnershipDTO ownershipHostory, Vehicle vehicle);
	
	public List<OwnershipHistoryDTO> findOwnerships(Long vehicleId);

}
