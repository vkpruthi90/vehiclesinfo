package com.vehicle.info.services;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;

import com.vehicle.info.dto.UserDTO_1;
import com.vehicle.info.dto.UserResponseDTO;
import com.vehicle.info.entities.User;

public interface IUserService {

	public UserResponseDTO findByUsername(String username);
	
	public UserDetails authUser(String username);
	
	public void persist(User user);
	
	public UserResponseDTO merge(UserDTO_1 user);
	
	public List<User> findUsers();
	
	public User findById(Long id);

}
