package com.vehicle.info.services;

import java.util.List;

import com.vehicle.info.dto.MaintenanceHistoryDTO;

public interface IMaintenanceHistoryService {

	public MaintenanceHistoryDTO merge(MaintenanceHistoryDTO maintenanceHistory);
	
	public MaintenanceHistoryDTO update(MaintenanceHistoryDTO maintenanceHistory, Long maintenanceId);
	
	public void deleteByMaintenanceId(Long maintenanceId);
	
	public List<MaintenanceHistoryDTO> findListByVehicleId(Long vehicleId);
}
