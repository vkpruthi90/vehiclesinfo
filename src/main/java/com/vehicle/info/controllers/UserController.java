package com.vehicle.info.controllers;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;

import com.vehicle.info.dto.LoginDTO;
import com.vehicle.info.dto.UserDTO_1;
import com.vehicle.info.dto.UserResponseDTO;
import com.vehicle.info.security.JwtTokenUtil;
import com.vehicle.info.services.IUserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private IUserService userService;
	
	@PostMapping()
	public ResponseEntity<?> persistUser(@RequestBody @Valid UserDTO_1 user, BindingResult result){
		if(result.hasErrors()) {
			return new ResponseEntity<>(result.getFieldError().getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }
		UserResponseDTO userResponseDTO = userService.merge(user);
		UserDetails userDetails = userService.authUser(userResponseDTO.getUsername());
		final String token = jwtTokenUtil.generateToken(userDetails);
		userResponseDTO.setAccessToken(token);
		return ResponseEntity.ok(userResponseDTO);		
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> loginUser(@RequestBody @Valid LoginDTO loginDTO, BindingResult result){
		if(result.hasErrors()) {
			return new ResponseEntity<>(result.getFieldError().getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }
		
		try {
			authenticate(loginDTO.getUsername(), loginDTO.getPassword());
		} catch (Exception e) {
			return new ResponseEntity<>("Invalid credentials", HttpStatus.UNAUTHORIZED);
		}
		UserResponseDTO user = userService.findByUsername(loginDTO.getUsername());
		UserDetails userDetails = userService.authUser(user.getUsername());
		final String token = jwtTokenUtil.generateToken(userDetails);
		user.setAccessToken(token);
		return ResponseEntity.ok(user);		
	}
	
	@GetMapping()
	public ResponseEntity<?> getUsers(){
		return ResponseEntity.ok(userService.findUsers());		
	}
	
	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}
