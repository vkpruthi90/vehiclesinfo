package com.vehicle.info.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vehicle.info.dto.MaintenanceHistoryDTO;
import com.vehicle.info.services.IMaintenanceHistoryService;

@RestController
@RequestMapping("/vehicles")
public class VehicleMaintenanceController {

	@Autowired
	IMaintenanceHistoryService maintenanceHistoryService;
	
	@PostMapping("/maintenance")
	public ResponseEntity<?> persistHistory(@RequestBody @Valid MaintenanceHistoryDTO maintenanceHistory,
			BindingResult result){
		if(result.hasErrors()) {
			return new ResponseEntity<>(result.getFieldError().getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }
		MaintenanceHistoryDTO maintenanceHistory2 = maintenanceHistoryService.merge(maintenanceHistory);
		return new ResponseEntity<>(maintenanceHistory2, HttpStatus.OK);
	}
	
	@GetMapping("/{vehicleId}/maintenance")
	public ResponseEntity<?> persistHistory(@PathVariable(name = "vehicleId") Long vehicleId){
		return new ResponseEntity<>(maintenanceHistoryService.findListByVehicleId(vehicleId), HttpStatus.OK);
	}
	
	@PostMapping("/maintenance/{maintenanceId}")
	public ResponseEntity<?> updateHistory(@PathVariable(name = "maintenanceId") Long maintenanceId,@RequestBody MaintenanceHistoryDTO maintenanceHistory){
		MaintenanceHistoryDTO maintenanceHistory2 = maintenanceHistoryService.update(maintenanceHistory, maintenanceId);
		return new ResponseEntity<>(maintenanceHistory2, HttpStatus.OK);
	}
	
	@DeleteMapping("/maintenance/{maintenanceId}")
	public ResponseEntity<?> deleteHistory(@PathVariable(name = "maintenanceId") Long maintenanceId){
		maintenanceHistoryService.deleteByMaintenanceId(maintenanceId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
}
