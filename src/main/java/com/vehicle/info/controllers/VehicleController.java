package com.vehicle.info.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vehicle.info.dto.OwnershipDTO;
import com.vehicle.info.dto.VehicleDTO;
import com.vehicle.info.entities.Vehicle;
import com.vehicle.info.services.IOwnershipHistoryService;
import com.vehicle.info.services.IVehicleService;

@RestController
@RequestMapping("/vehicles")
public class VehicleController {

	@Autowired
	private IVehicleService vehicleService;
	
	@Autowired
	private IOwnershipHistoryService ownershipHistoryService;
	
	@PostMapping()
	public ResponseEntity<?> persistVehicle(@RequestBody @Valid VehicleDTO vehicle, BindingResult result){
		if(result.hasErrors()) {
			return new ResponseEntity<>(result.getFieldError().getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }
		VehicleDTO vehicle2 = vehicleService.merge(vehicle);
		return new ResponseEntity<VehicleDTO>(vehicle2, HttpStatus.OK);
	}
	
	@GetMapping()
	public ResponseEntity<List<Vehicle>> getVehicles(){
		return new ResponseEntity<List<Vehicle>>(vehicleService.findAll(),HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Vehicle> getVehicleById(@PathVariable(name = "id") Long id){
		return new ResponseEntity<Vehicle>(vehicleService.findById(id),HttpStatus.OK);
	}
	
	@PutMapping("/owners")
	public ResponseEntity<?> addNewOwner(@RequestBody @Valid OwnershipDTO ownership, BindingResult result){
		if(result.hasErrors()) {
			return new ResponseEntity<>(result.getFieldError().getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }
		VehicleDTO vehicle2 = vehicleService.addNewOwner(ownership);
		return new ResponseEntity<VehicleDTO>(vehicle2, HttpStatus.OK);
	}
	
	@GetMapping("/{vehicleId}/owners")
	public ResponseEntity<?> getOwners(@PathVariable(name = "vehicleId") Long vehicleId){
		return new ResponseEntity<>(ownershipHistoryService.findOwnerships(vehicleId), HttpStatus.OK);
	}
	
}
