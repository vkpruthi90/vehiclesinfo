package com.vehicle.info.dto;

import javax.validation.constraints.NotNull;

public class OwnershipDTO {

	@NotNull(message = "vehicleId is required")
	private Long vehicleId;
	@NotNull(message = "ownerId is required")
	private Long ownerId;
	@NotNull(message = "transferredOn is required")
	private String transferredOn;
	@NotNull(message = "comments is required")
	private String comments;
	
	public Long getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(Long vehicleId) {
		this.vehicleId = vehicleId;
	}
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public String getTransferredOn() {
		return transferredOn;
	}
	public void setTransferredOn(String transferredOn) {
		this.transferredOn = transferredOn;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
}
