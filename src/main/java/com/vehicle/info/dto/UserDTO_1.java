package com.vehicle.info.dto;

import javax.validation.constraints.NotNull;

public class UserDTO_1 {
	
	@NotNull(message = "username is required")
	private String username;
	@NotNull(message = "First name is required")
	private String firstName;
	@NotNull(message = "Last name is required")
	private String lastName;
	@NotNull(message = "password is required")
	private String password;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
