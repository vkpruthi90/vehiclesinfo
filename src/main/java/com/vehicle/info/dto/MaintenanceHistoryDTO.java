package com.vehicle.info.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class MaintenanceHistoryDTO {

	private Long id;
	@NotNull(message = "comments is required")
	private String comments;
	@NotNull(message = "maintainedOn is required")
	private Date maintainedOn;
	@NotNull(message = "cost is required")
	private Double cost;
	@NotNull(message = "vehicle is required")
	private VehicleDTO_1 vehicle;
	@NotNull(message = "user is required")
	private UserDTO user;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public Date getMaintainedOn() {
		return maintainedOn;
	}
	public void setMaintainedOn(String maintainedOn) {
		try {
			this.maintainedOn = new SimpleDateFormat("dd-MM-yyyy").parse(maintainedOn);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public VehicleDTO_1 getVehicle() {
		return vehicle;
	}
	public void setVehicle(VehicleDTO_1 vehicle) {
		this.vehicle = vehicle;
	}
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
}
