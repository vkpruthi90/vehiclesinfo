package com.vehicle.info.dto;

import java.util.Date;


public class OwnershipHistoryDTO {

	private Long id;
	private String comments;
	private Date transferredOn;
	private VehicleDTO_1 vehicle;
	private UserDTO owner;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Date getTransferredOn() {
		return transferredOn;
	}
	public void setTransferredOn(Date transferredOn) {
		this.transferredOn = transferredOn;
	}
	public VehicleDTO_1 getVehicle() {
		return vehicle;
	}
	public void setVehicle(VehicleDTO_1 vehicle) {
		this.vehicle = vehicle;
	}
	public UserDTO getOwner() {
		return owner;
	}
	public void setOwner(UserDTO owner) {
		this.owner = owner;
	}
}
