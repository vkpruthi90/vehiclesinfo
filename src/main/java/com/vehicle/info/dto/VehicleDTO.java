package com.vehicle.info.dto;

import javax.validation.constraints.NotNull;

public class VehicleDTO {
	
	private Long id;
	@NotNull(message = "manufacturer is required")
	private String manufacturer;
	@NotNull(message = "model is required")
	private String model;
	@NotNull(message = "year is required")
	private String year;
	@NotNull(message = "variant is required")
	private String variant;
	private UserDTO owner;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getVariant() {
		return variant;
	}
	public void setVariant(String variant) {
		this.variant = variant;
	}
	public UserDTO getOwner() {
		return owner;
	}
	public void setOwner(UserDTO owner) {
		this.owner = owner;
	}
}
