package com.vehicle.info.exception.handler;

public class HistoryNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HistoryNotFoundException(Long id) {

        super(String.format("Owner with Id %d not found", id));
    }
}
