package com.vehicle.info.exception.handler;

public class VehicleNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VehicleNotFoundException(Long id) {

        super(String.format("Vehicle with Id %d not found", id));
    }
}
