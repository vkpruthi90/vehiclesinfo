### Vehicle Maintenance And Ownership Details

This application is developed in **Spring Boot 2.4**.
The Complete tech stack is as follow.

* Java 8.0
* Spring Boot 2.4
* Spring Security (JWT)
* H2 (In Memory Database)
---

### How to run the app?

* Create runnable jar `mvn clean install`
* Find jar in target folder and run below command
`java -jar VehiclesInfo-0.0.1-SNAPSHOT.jar`
---

## REST API Calls

 * **Create User**  
API:  http://localhost:8080/users  
Method: POST  
Data:
        ```{
      "username":"test2", 
      "firstName":"Test",
      "lastName":"User",
      "password":"test@123"
    }```


-  **User Login**  
API: http://localhost:8080/users/login  
Method: POST  
Data:  
         ```{
          "username":"test2",
          "password":"test@123"
        }```

- **Add Vehicle**  
API: http://localhost:8080/vehicles  
Method: POST  
Headers:  
        ```Content-Type:application/json
        Authorization: Bearer jwt_token```
Data:  
           ```{
          "manufacturer":"BMW",
          "model":"330i",
          "year": "2020",
          "variant":"Sport``"
  	  }```

- **Add Vehicle Owner/ Transfer Ownership**  
API: http://localhost:8080/vehicles/owners  
Method: PUT  
Headers:  
        ```Content-Type:application/json
        Authorization: Bearer jwt_token```
Data:  
              ```{
          "vehicleId":1,
          "ownerId":1,
          "transferredOn":"11-12-2020",
          "comments":"test data"
}```

* **Get Vehicle Owners List**  
API: http://localhost:8080/vehicles/1/owners  
Method: GET  
Headers:   
       ``` Authorization: Bearer jwt_token```

* **Add Maintenance History**  
API: http://localhost:8080/vehicles/maintenance  
Method: POST  
Headers:  
        ```Content-Type:application/json
        Authorization: Bearer jwt_token```
Data:  
      ```{
         "comments":"Test data",
         "maintainedOn": "12-12-2020",
         "cost":10.2,
         "vehicle":{"id":1},
         "user":{"id":1}	
    }```

*  **Get Maintenance History**  
API: http://localhost:8080/vehicles/1/maintenance  
Method: GET  
Headers:  
        ```Authorization: Bearer jwt_token```

*  **Get Maintenance History By ID**  
API: http://localhost:8080/vehicles/maintenance/1  
Method: GET  
Headers:  
        ```Authorization: Bearer jwt_token```

-  **DELETE Maintenance History By ID**  
API: http://localhost:8080/vehicles/maintenance/1  
Method: DELETE  
Headers:  
        ```Authorization: Bearer jwt_token```

